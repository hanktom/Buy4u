package com.tom.buy4u.client;

import android.text.TextUtils;
import android.widget.ImageView;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by hank on 2018/1/14.
 */
public class Item implements Serializable{
    public static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy/MM/dd");
    public static final String TABLE = "items";
    private int id;
    private int groupId;
    private String name;
    private String description;
    private String photoPath;
    private String photoUrl;
    private Date start;
    private String startFormatted;
    private Date end;
    private String endFormatted;
    private int price;
    String priceString;
    private int qty;
    private String qtyString;

    public String getPriceString() {
        if (price != 0) {
            priceString = String.valueOf(price);
        }
        return priceString;
    }

    public void setPriceString(String priceString) {
        this.priceString = priceString;
        try {
            price = Integer.parseInt(priceString);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            price = 0;
        }
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", groupId=" + groupId +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", photoPath='" + photoPath + '\'' +
                ", start=" + start +
                ", end=" + end +
                ", price=" + price +
                ", qty=" + qty +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStart() {
        if (start == null){
            if (TextUtils.isEmpty(startFormatted)){
                start = new Date();
            }else{
                try {
                    start = SDF.parse(startFormatted);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        if (end == null){
            if (TextUtils.isEmpty(endFormatted)){
                end = new Date();
            }else{
                try {
                    end = SDF.parse(endFormatted);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public String getStartFormatted() {
        startFormatted = SDF.format(getStart());
        return startFormatted;
    }

    public void setStartFormatted(String startFormatted) {
        this.startFormatted = startFormatted;
    }

    public String getEndFormatted() {
        endFormatted = SDF.format(getEnd());
        return endFormatted;
    }

    public void setEndFormatted(String endFormatted) {
        this.endFormatted = endFormatted;
    }

    public String getQtyString() {
        if (qty != 0) {
            qtyString = qty+"";
        }
        return qtyString;
    }

    public void setQtyString(String qtyString) {
        if (TextUtils.isEmpty(qtyString)){
            qtyString = "0";
        }
        this.qtyString = qtyString;
        if (!TextUtils.isEmpty(qtyString)){
            qty = Integer.parseInt(qtyString);
        }
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
