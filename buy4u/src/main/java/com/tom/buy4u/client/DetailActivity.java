package com.tom.buy4u.client;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Map;

public class DetailActivity extends AppCompatActivity {

    private Item item;
    private String uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        item = (Item) getIntent().getSerializableExtra("ITEM");
        FirebaseAuth auth = FirebaseAuth.getInstance();
        uid = auth.getCurrentUser().getUid();
    }

    public void buy(View view){
        //
        Order order = new Order(uid, item.getId(), item.getGroupId(), 1);
        DatabaseReference orders = FirebaseDatabase.getInstance()
                .getReference("buyers")
                .child(uid)
                .child("orders").push();
        orders.setValue(order);
    }
}
