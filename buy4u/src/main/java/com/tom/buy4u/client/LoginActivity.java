package com.tom.buy4u.client;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks {

    private static final int RC_GOOGLE_SIGN_IN = 100;
    private static final String TAG = LoginActivity.class.getSimpleName();
    private EditText edUserid;
    private EditText edPassword;
    private FirebaseAuth auth;
    private GoogleApiClient apiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        auth = FirebaseAuth.getInstance();
        edUserid = findViewById(R.id.ed_userid);
        edPassword = findViewById(R.id.ed_password);
        GoogleSignInOptions gso =
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.web_application_client_id))
                .requestEmail()
                .build();
        apiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        apiClient.connect();
        apiClient.registerConnectionCallbacks(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        apiClient.disconnect();
        apiClient.unregisterConnectionCallbacks(this);
    }

    public void login(View view){
        String userid = edUserid.getText().toString();
        String passwd = edPassword.getText().toString();

        auth.signInWithEmailAndPassword(userid, passwd)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            Toast.makeText(LoginActivity.this, "Success", Toast.LENGTH_LONG).show();
                            setResult(RESULT_OK);
                            finish();
                        }else{
                            Toast.makeText(LoginActivity.this, "Failed", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public void google(View view){
        Intent signIntent = Auth.GoogleSignInApi.getSignInIntent(apiClient);
        startActivityForResult(signIntent, RC_GOOGLE_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_GOOGLE_SIGN_IN){
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount account = result.getSignInAccount();
                Log.d(TAG, "onActivityResult: " + account.getEmail());
                Log.d(TAG, "onActivityResult: " + account.getIdToken());
                firebaseAuthWithGoogle(account);
            }else{
                Log.d(TAG, "onActivityResult: " + result.getStatus().toString());
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        auth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    Log.d(TAG, "firebaseAuthWithGoogle: ");
                    Toast.makeText(LoginActivity.this,
                            "Sign in Google success", Toast.LENGTH_LONG).show();
                    setResult(RESULT_OK);
                    finish();
                }else{
                    Toast.makeText(LoginActivity.this,
                            "Sign in Google failed", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
