package com.litto.buy4u;

import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jph.takephoto.app.TakePhoto;
import com.jph.takephoto.app.TakePhotoActivity;
import com.jph.takephoto.model.CropOptions;
import com.jph.takephoto.model.TResult;
import com.jph.takephoto.model.TakePhotoOptions;
import com.litto.buy4u.databinding.ActivityAddBinding;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class AddActivity extends TakePhotoActivity {

    private static final String TAG = AddActivity.class.getSimpleName();
    private ImageView photo;
    /*private TextView prevName;
    private TextView info;
    private EditText name;
    private EditText start;
    private EditText end;
    private EditText price;
    private EditText qty;
    private Button bSelectImage;
    private Button bDone;*/

    ActivityAddBinding binding;
    private String imagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add);
        Item item = new Item();
        item.setName("北海道巧克力");
        item.setPrice(300);
        binding.setItem(item);
        findViews();

    }

    private void findViews() {
        photo = findViewById(R.id.prev_photo);
        /*
        prevName = findViewById(R.id.prev_name);
        info = findViewById(R.id.prev_info);
        name = findViewById(R.id.name);
        start = findViewById(R.id.start);
        end = findViewById(R.id.end);
        price = findViewById(R.id.price);
        qty = findViewById(R.id.qty);
        /*
        bSelectImage = findViewById(R.id.b_select_image);
        bSelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickPhoto();
            }
        });
//        bDone = findViewById(R.id.b_done);

        /*
        bDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: " + binding.getItem());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                final Item item = new Item();
                item.setName(name.getText().toString());
                try {
                    item.setStart(sdf.parse(start.getText().toString()));
                    item.setEnd(sdf.parse(end.getText().toString()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                item.setPrice(Integer.parseInt(price.getText().toString()));
                item.setQty(Integer.parseInt(qty.getText().toString()));
                item.setPhotoPath(imagePath);
                //
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ItemDatabase.getDatabase(AddActivity.this)
                                .itemDao().insert(item);
                    }
                }).start();

            }
        });*/
    }

    public void done(View view){
        Log.d(TAG, "onClick: " + binding.getItem());
        final Item item = binding.getItem();
        item.setPhotoPath(imagePath);
        new Thread(new Runnable() {
            @Override
            public void run() {
                ItemDatabase.getDatabase(AddActivity.this)
                        .itemDao().insert(item);
                setResult(RESULT_OK);
            }
        }).start();
    }

    public void pickPhoto(View view) {
        File file = new File(Environment.getExternalStorageDirectory(),
                "/buy4u/" + System.currentTimeMillis()+".jpg");
        if (!file.getParentFile().exists()){
            file.getParentFile().mkdirs();
        }
        Uri uri = Uri.fromFile(file);
        TakePhoto takePhoto = getTakePhoto();
        takePhoto.setTakePhotoOptions(new TakePhotoOptions.Builder()
                .setWithOwnGallery(true)
                .create());
        takePhoto.onPickFromGalleryWithCrop(uri, getCropOptions());

    }

    private CropOptions getCropOptions() {
        int width = 600;
        int height = 720;
        CropOptions.Builder builder = new CropOptions.Builder()
                .setAspectX(width)
                .setAspectY(height)
                .setWithOwnCrop(true);
        return builder.create();
    }

    @Override
    public void takeSuccess(TResult result) {
        super.takeSuccess(result);
        imagePath = result.getImage().getOriginalPath();
        Glide.with(this)
                .load(imagePath)
                .into(photo);
    }

    @Override
    public void takeFail(TResult result, String msg) {
        super.takeFail(result, msg);
    }

    @Override
    public void takeCancel() {
        super.takeCancel();
    }
}
