package com.litto.buy4u;

/**
 * Created by hank on 2018/1/21.
 */

public class Contact {
    String name;
    String phone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
